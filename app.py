from flask import Flask
from flaskext.mysql import MySQL
import unicodedata as ud
from flask import request, jsonify,json
from flask_cors import CORS
import re, sys, datetime, os, config, requests

app = Flask(__name__)
mysql = MySQL()
CORS(app)
# MySQL configurations

# app.config['MYSQL_DATABASE_USER'] = 'coach'
# app.config['MYSQL_DATABASE_PASSWORD'] = 'Aspire4720z!'
# app.config['MYSQL_DATABASE_DB'] = 'smartcart'
# app.config['MYSQL_DATABASE_HOST'] = '103.3.62.188'
config_param = {
    "development": "config.DevelopmentConfig",
    "production": "config.ProductionConfig",
    "testing": "config.TestingConfig",
    "default": "config.DevelopmentConfig"
}

# app.config['MYSQL_DATABASE_USER'] = 
# app.config['MYSQL_DATABASE_PASSWORD'] = ''
# app.config['MYSQL_DATABASE_DB'] = 'smartcart'
# app.config['MYSQL_DATABASE_HOST'] = 'localhost'


def configure_app(app):
    config_name = os.getenv('FLASK_CONFIGURATION', 'production')
    app.config.from_object(config_param[config_name]) 
    print("success")

configure_app(app)

mysql.init_app(app)
conn = mysql.connect()
cur = conn.cursor()

@app.route('/process')
def process():
	store = 1
	_file = request.args.get('file')

	for path, dirs, files in os.walk('spl'):
		for filename in files: 
			fullpath = os.path.join(path, filename)
			# if filename.endswith(".spl") :
			if filename.endswith(".SPL") or filename.endswith(".spl"):
				with open(fullpath, encoding="utf-8", errors='ignore') as file:
					data = file.read()
					y = data.replace("\x00",'')
					y = re.sub('\s+', ' ', y).strip()
					y = y.replace("\n",'')
					match = re.search(r"Closed",y)

					if match:
						y = re.sub(r"========================================.+",'',y)
						get_product = re.findall(r"([0-9]+)\s([0-9a-zA-Z\s\-\/]{2,})\s([0-9\,]+)", y)
						timestamp = re.search(r"([0-9]{2,2}\/[0-9]{2,2}\/[0-9]{4,4}\s)([0-9]{2,2}\:[0-9]{2,2})",y).groups()
						timestamp = ''.join(timestamp)
						date_time_obj = datetime.datetime.strptime(timestamp, '%d/%m/%Y %H:%M')

						cashier = re.search(r"(POS\:)([a-zA-Z0-9]+)",y).groups()
						meta1 = re.search(r"(TABLE\:[0-9A-Za-z]+)",y).groups()
						meta2 = re.search(r"((Pax\:)([a-zA-Z0-9]+))\s+((OP:)([a-zA-Z\s]+))",y).groups()
						meta_nn = meta1[0]+";"+meta2[0]+";"+meta2[3]
						
						content_part = y.split('----------------------------------------')
						part_payment = content_part[-1]
						if  re.match(r"Signature",y) or len(content_part)==3: 
							part_2 = content_part[2]
						elif len(content_part)==5: 
							part_2 = content_part[4]
						else:
							part_2 = content_part[2]

						# get_total = re.findall(r"([A-Z\s]{2,})([0-9\,]+)",part_3)

						total = re.search(r"(TOTAL\s)([0-9\,]+)", part_payment).groups()
						total = total[1].replace(',','').strip();
						part_payment = re.sub(r"(TOTAL\s)([0-9\,]+)", "", part_payment)

						# if re.match(r"Signature",y) or len(content_part)==3:

						method = re.search(r"([A-Za-z\s]{2,})\s([0-9\,]{2,})", part_payment)
						if method:
							method = method.groups()
							method = method[0].lstrip()
						else:
							method = "-"


						meta_prices = re.findall(r"([A-Za-z\s]{2,}\s)([0-9\,]+)",part_2)
						field_meta = ""
						list_trash = ['Ru z','T o','l o','o','Fo','DAo','Ko','Go','Xo','Jo','uo','v','vaQv','vRv','Rxu']
						for meta_price in meta_prices:

							field_meta += ':'.join(meta_price)+";"

						queryMaster = "INSERT INTO tr_shop_master(id_kasir, id_member, type, receipt_date,field_1, total, payment_method, field_2, spl_file,entry_stamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
						paramMaster = (1,1,1,date_time_obj, meta_nn, total,method, field_meta, fullpath, datetime.datetime.now())
						cur.execute(queryMaster, paramMaster);
						id_master = cur.lastrowid
						conn.commit()

						queryDetail = "INSERT INTO tr_shop_detail(id_master, product_name,qty, price,entry_stamp) VALUES(%s,%s,%s,%s,%s)"
						
						for product in get_product:
							param = (id_master, product[1].rstrip(),product[0], product[2].replace(",",""), datetime.datetime.now())
							cur.execute(queryDetail, param);
							conn.commit()
					
		continue

	conn.close()
	return 'success'

@app.route('/processSingle')
def processSingle(_file=''):
	store = 1
	_file = _file if _file != '' else request.args.get('file')
	fullpath = "/home/root/www/osmosis/api/spl/"+_file
	_filename = _file.split('/')
	_clear_dir = _file.split('.')
	_file_dir = _clear_dir[0].split('_')


	with open(fullpath, encoding="utf-8", errors='ignore') as file:
		data = file.read()
		y = data.replace("\x00",'')
		y = re.sub('\s+', ' ', y).strip()
		y = y.replace("\n",'')
		match = re.search(r"Closed",y)
		directory = ""

		if match:
			get_date_stamp = re.search(r"<-----------([0-9\/\s\:]+)----------->", y).groups()
			y = re.sub(r"========================================.+",'',y)
			get_product = re.findall(r"([0-9]+)\s([0-9a-zA-Z\s\-\/]{2,})\s([0-9\,]+)", y)
			timestamp = re.search(r"([0-9]{2,2}\/[0-9]{2,2}\/[0-9]{4,4}\s)([0-9]{2,2}\:[0-9]{2,2})",y).groups()
			timestamp = ''.join(timestamp)
			date_time_obj = datetime.datetime.strptime(get_date_stamp[0], '%d/%m/%Y %H:%M')

			cashier = re.search(r"(POS\:)([a-zA-Z0-9]+)",y).groups()
			meta1 = re.search(r"(TABLE\:[0-9A-Za-z]+)",y).groups()
			meta2 = re.search(r"((Pax\:)([a-zA-Z0-9]+))\s+((OP:)([a-zA-Z\s]+))",y).groups()
			meta_nn = meta1[0]+";"+meta2[0]+";"+meta2[3]
			
			content_part = y.split('----------------------------------------')
			part_payment = content_part[-1]
			if  re.match(r"Signature",y) or len(content_part)==3: 
				part_2 = content_part[2]
			elif len(content_part)==5: 
				part_2 = content_part[4]
			else:
				part_2 = content_part[2]

			# get_total = re.findall(r"([A-Z\s]{2,})([0-9\,]+)",part_3)

			total = re.search(r"(TOTAL\s)([0-9\,]+)", part_payment).groups()
			total = total[1].replace(',','').strip();
			part_payment = re.sub(r"(TOTAL\s)([0-9\,]+)", "", part_payment)

			# if re.match(r"Signature",y) or len(content_part)==3:
			method = re.search(r"([A-Za-z\s]{2,})\s([0-9\,]{2,})", part_payment)
			if method:
				method = method.groups()
				method = method[0].lstrip()
			else:
				method = "-"


			meta_prices = re.findall(r"([A-Za-z\s]{2,}\s)([0-9\,]+)",part_2)
			field_meta = ""
			list_trash = ['Ru z','T o','l o','o','Fo','DAo','Ko','Go','Xo','Jo','uo','v','vaQv','vRv','Rxu']
			for meta_price in meta_prices:

				field_meta += ':'.join(meta_price)+";"

			queryMaster = "INSERT INTO tr_shop_master(id_kasir, id_member, type, receipt_date,field_1, total, payment_method, field_2, spl_file, entry_stamp) VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
			paramMaster = (1,1,1,date_time_obj, meta_nn, total,method, field_meta, fullpath, datetime.datetime.now())
			cur.execute(queryMaster, paramMaster);
			id_master = cur.lastrowid
			conn.commit()
			queryDetail = "INSERT INTO tr_shop_detail(id_master, product_name,qty, price, entry_stamp) VALUES(%s,%s,%s,%s,%s)"

			for product in get_product:
				param = (id_master, product[1].rstrip(),product[0], product[2].replace(",",""), datetime.datetime.now())
				cur.execute(queryDetail, param);
				conn.commit()

			directory += "/home/root/www/osmosis/api/spl/"+_file_dir[-2]+"/"+_file_dir[-1]+"/"+str(date_time_obj.year)+"/"+str(date_time_obj.month)+"/"+str(date_time_obj.day)
			
			if not os.path.exists(directory): 
				original_umask = os.umask(0)
				os.makedirs(directory,  mode=0o777)

			file.close()
			os.rename(fullpath, directory+"/"+_filename[-1])	

			dataJson = getJson(id_master)
			objJson = json.loads(dataJson)
			store = objJson['store_name'].replace("\s","_")

			res = requests.post('http://bitech2:9Hmw+$D854Eh3gGD6@159.65.142.133:8088/smartcart/transaction', data=getJson(id_master), headers={"content-type":"application/JSON"})
			data_response = json.loads(res._content)
			queryUpdateES = "UPDATE tr_shop_master SET field_3 = %s WHERE id = %s"
			queryUpdateES = cur.execute(queryUpdateES, (data_response['_id'], id_master))
			conn.commit()	

		else:
			file.close()
			os.rename(fullpath, "/home/root/www/osmosis/api/spl/junk/"+_filename[-1])	
	return 'success'
# @app.route('/getJson')

def getJson(id):
	queryMaster = cur.execute('''	SELECT 
						c.store_name, 
						b.id pos,
						a.entry_stamp, 
						a.receipt_date,
						a.field_1 raw_top,
						a.field_2 raw_bottom,
						a.payment_method,
						a.total,
						a.id
					FROM tr_shop_master a 
					LEFT JOIN ms_cashier b ON a.id_kasir = b.id 
					LEFT JOIN ms_store c ON b.id_store = c.id 
					WHERE a.id = %s ''', (id))


	data_master = cur.fetchone()
	row_item = [x[0] for x in cur.description];
	data_master = dict(zip(row_item,data_master))

	result_item = cur.execute("SELECT product_name,qty, price FROM tr_shop_detail WHERE id_master = %s",(id))

	result_item = cur.fetchall()

	row_item = [x[0] for x in cur.description]

	item_data = []
	for item in result_item:
		data_item = dict(zip(row_item,item))
		item_data.append(data_item)

	data_master['item'] = item_data;
	# print(json.dumps(data_master))
	return json.dumps(data_master);

@app.route('/openData')
def openData():
	cur.execute('''	SELECT 
						c.store_name, 
						b.id pos,
						a.entry_stamp, 
						a.receipt_date,
						a.field_1 raw_top,
						a.field_2 raw_bottom,
						a.field_3 id_elk,
						a.payment_method,
						a.total,
						a.id
					FROM tr_shop_master a 
					LEFT JOIN ms_cashier b ON a.id_kasir = b.id 
					LEFT JOIN ms_store c ON b.id_store = c.id 
					order by receipt_date DESC LIMIT 0, 200''')

	row_headers=[x[0] for x in cur.description] #this will extract row headers
	rv = cur.fetchall()

	json_data=[]
	for result in rv:
		_data = dict(zip(row_headers,result))
		if  _data['entry_stamp']!= None:
			_data['entry_stamp'] = _data['entry_stamp'].strftime('%Y-%m-%d %H:%M:%S')
		_data['receipt_date'] = _data['receipt_date'].strftime('%Y-%m-%d %H:%M:%S')
		result_item = cur.execute("SELECT product_name,qty, price FROM tr_shop_detail WHERE id_master = "+str(_data['id']))
		result_item = cur.fetchall()
		row_item = [x[0] for x in cur.description];

		item_data = []
		for item in result_item:
			data_item = dict(zip(row_item,item))
			item_data.append(data_item)
		_data['item'] = item_data;
		json_data.append(_data)
	return json.dumps(json_data)

@app.route('/transferRawData')
def transferRawData():
	cur.execute('''	SELECT 
						a.id
						
					FROM tr_shop_master a 
					order by receipt_date ASC LIMIT 1400,50''')

	row_headers=[x[0] for x in cur.description] #this will extract row headers
	rv = cur.fetchall()
	

	for result in rv:
		_data = dict(zip(row_headers,result))
		dataJson = getJson(_data['id'])
		
		res = requests.post('http://bitech2:9Hmw+$D854Eh3gGD6@159.65.142.133:8088/smartcart/transaction', data=dataJson, headers={"content-type":"application/JSON"})

		data_response = json.loads(res._content)
		queryUpdateES = "UPDATE tr_shop_master SET field_3 = %s WHERE id = %s"
		queryUpdateES = cur.execute(queryUpdateES, (data_response['_id'], _data['id']))
		conn.commit()	
		
	return ""

if __name__ == '__main__':
    app.run(host='103.3.62.188')