import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'this-really-needs-to-be-changed'


class ProductionConfig(Config):
    DEBUG = False
    MYSQL_DATABASE_USER = 'coach'
    MYSQL_DATABASE_PASSWORD = 'Aspire4720z!'
    MYSQL_DATABASE_DB = 'smartcart'
    MYSQL_DATABASE_HOST = 'localhost'

class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
    MYSQL_DATABASE_USER = 'root'
    MYSQL_DATABASE_PASSWORD = ''
    MYSQL_DATABASE_DB = 'smartcart'
    MYSQL_DATABASE_HOST = 'localhost'

class TestingConfig(Config):
    TESTING = True